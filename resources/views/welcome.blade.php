<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <style>
        @font-face {
        font-family: "Open Sans Regular";
        src: url("...");
        font-display: swap;
}
        </style>


        <!-- Styles -->



    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">


            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <table class="table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Family</th>
                        <th scope="col">Category</th>
                        <th scope="col">Kind</th>
                        <th scope="col">Variants</th>
                        <th scope="col">Subsets</th>
                        <th scope="col">Files</th>
                        <th scope="col">Example</th>

                      </tr>
                    </thead>
                    <tbody>


                      @foreach ($responseBody->items as $item)


                      <style>
                        @font-face {
                        font-family: {{$item->family}};
                        src: url({{collect($item->files)->first()}});
                        font-display: swap;
                        }
                      </style>

                      <tr>

                        <td>{{$item->family}}</td>
                        <td>{{$item->category}}</td>
                        <td>{{$item->kind}}</td>
                        <td>
                            @foreach ($item->variants as $v)
                            {{$v}} ,
                            @endforeach
                        </td>
                        <td>
                            @foreach ($item->subsets as $subset)
                            {{$subset}} ,
                            @endforeach
                        </td>
                        </td>
                        <td>
                            @foreach ($item->files as $file)

                            {{$file}} ,
                            @endforeach
                        </td>

                        <td style="font-family:'{{$item->family}}', {{$item->category}};"> Lorem ipsum dolor sit amet.</td>
                      </tr>

                      @endforeach
                    </tbody>
                  </table>

            </div>
        </div>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
